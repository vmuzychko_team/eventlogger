package net.eda.repository;

import net.eda.data.LogRecord;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.data.mongodb.repository.Query;

import java.util.List;

public interface LogRepository extends MongoRepository<LogRecord, String>
{
    @Query(value="{ 'eventId' : ?0 }")
    public List<LogRecord> findByEventId(long eventId);
}
