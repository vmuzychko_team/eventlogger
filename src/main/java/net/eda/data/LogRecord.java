package net.eda.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.Indexed;

import java.time.LocalDateTime;

public class LogRecord
{
    @Id
    private String id;
    private String message;
    private LocalDateTime timestamp;
    @Indexed
    private long eventId;
    private String event;

    public LogRecord(String message, LocalDateTime timestamp, long eventId, String event)
    {
        this.message = message;
        this.timestamp = timestamp;
        this.eventId = eventId;
        this.event = event;
    }

    public String getId()
    {
        return id;
    }

    public String getMessage()
    {
        return message;
    }

    public LocalDateTime getTimestamp()
    {
        return timestamp;
    }

    public long getEventId()
    {
        return eventId;
    }

    public String getEvent()
    {
        return event;
    }
}

