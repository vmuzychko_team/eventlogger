package net.eda;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;
import net.eda.log.Logger;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.io.IOException;
import java.util.concurrent.TimeoutException;

@SpringBootApplication
public class LoggerApplication
{
    public static final String ALL_EVENTS = "";
    @Value("${host.name:localhost}")
    public String hostName;
    @Value("${new.events.exchange:new.events}")
    private String newEventsExchange;
    @Value("${validation.exchange:validated.events}")
    private String validatedEventsExchange;
    @Value("${logging.queue:log}")
    private String loggingQueue;

    @Bean
    Connection connection()
    {
        ConnectionFactory connectionFactory = new ConnectionFactory();
        connectionFactory.setHost(hostName);
        try
        {
            return connectionFactory.newConnection();
        }
        catch (IOException | TimeoutException e)
        {
            throw new BeanCreationException("Unable to establish connection.", e);
        }
    }

    @Bean
    Channel channel(Connection connection)
    {
        try
        {
            Channel channel = connection.createChannel();
            channel.queueDeclare(loggingQueue, true, false, false, null);
            channel.queueBind(loggingQueue, newEventsExchange, ALL_EVENTS);
            channel.queueBind(loggingQueue, validatedEventsExchange, ALL_EVENTS);
            return channel;
        }
        catch (IOException e)
        {
            throw new BeanCreationException("Unable to create a channel.", e);
        }
    }

    @Bean(destroyMethod = "preDestroy")
    Logger logger(Connection connection, Channel channel) {
        return new Logger(connection, channel, loggingQueue);
    }

    public static void main(String[] args)
    {
        SpringApplication.run(LoggerApplication.class, args);
    }
}
