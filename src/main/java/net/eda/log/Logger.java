package net.eda.log;

import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;
import net.eda.data.LogRecord;
import net.eda.exception.EdaException;
import net.eda.models.Event;
import net.eda.repository.LogRepository;
import org.springframework.beans.factory.BeanCreationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.time.LocalDateTime;
import java.util.concurrent.TimeoutException;

@Service
public class Logger extends DefaultConsumer
{
    private Connection connection;

    @Autowired
    LogRepository logRepository;

    public Logger(Connection connection, Channel channel, String loggingQueue)
    {
        super(channel);
        try
        {
            channel.basicConsume(loggingQueue, false, this);
            this.connection = connection;
        }
        catch (IOException e)
        {
            throw new BeanCreationException("Unable to instantiate Logger.", e);
        }
    }

    @Override
    public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
        throws IOException
    {
        getChannel().basicAck(envelope.getDeliveryTag(), false);
        Event event = new Event(body);
        LogRecord logRecord = new LogRecord("logged", LocalDateTime.now(), event.getId(), event.toString());
        logRepository.save(logRecord);
        System.out.println("== logged == log id == " + logRecord.getId() + " == " + event + " =======");
    }

    public void preDestroy()
    {
        try
        {
            getChannel().close();
            connection.close();
        }
        catch (IOException | TimeoutException e)
        {
            throw new EdaException("Unable to destroy Logger", e);
        }
    }
}
