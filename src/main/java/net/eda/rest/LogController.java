package net.eda.rest;

import net.eda.data.LogRecord;
import net.eda.repository.LogRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/log")
public class LogController
{
    @Autowired
    private LogRepository logRepository;

    @GetMapping("/{id}")
    public LogRecord getLogById(@PathVariable String id)
    {
        return logRepository.findOne(id);
    }

    @GetMapping("/event/{eventId}")
    public List<LogRecord> getLogsByEventId(@PathVariable long eventId)
    {
        return logRepository.findByEventId(eventId);
    }
}
